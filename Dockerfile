FROM openjdk:17-jdk-alpine
COPY ./target/brain-station-test.jar .
ENTRYPOINT ["java","-jar","brain-station-test.jar"]
EXPOSE 8080