package com.example.brainstationtest.controllers;

import com.example.brainstationtest.dto.UserDto;
import com.example.brainstationtest.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;


    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody UserDto dto) {
        if (userService.getUserByUsername(dto.getUsername())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Username Already Exist");
        } else {
            return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(dto));
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        UserDto dto = userService.getUser(id);
        return dto == null
                ? ResponseEntity.status(HttpStatus.NOT_FOUND).build()
                : ResponseEntity.status(HttpStatus.FOUND).body(dto);
    }

    @GetMapping("/get-all")
    public ResponseEntity<?> getAllUser() {
        List<UserDto> userDtoList = userService.getAllUser();
        return userDtoList.isEmpty()
                ? ResponseEntity.status(HttpStatus.NOT_FOUND).build()
                : ResponseEntity.status(HttpStatus.OK).body(userDtoList);
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody UserDto dto) {
        UserDto userDto = userService.getUser(dto.getId());
        return userDto == null
                ? ResponseEntity.status(HttpStatus.NOT_FOUND).build()
                : ResponseEntity.status(HttpStatus.OK).body(userService.update(dto));

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        UserDto userDto = userService.getUser(id);
        if (userDto == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            userService.delete(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

    }
}