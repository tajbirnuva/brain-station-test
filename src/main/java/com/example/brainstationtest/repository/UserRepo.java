package com.example.brainstationtest.repository;

import com.example.brainstationtest.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User,Long> {
    boolean existsByUsername(String username);
}
