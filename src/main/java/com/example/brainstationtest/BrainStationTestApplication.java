package com.example.brainstationtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BrainStationTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrainStationTestApplication.class, args);
    }

}
