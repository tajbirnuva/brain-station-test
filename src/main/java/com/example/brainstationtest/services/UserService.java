package com.example.brainstationtest.services;

import com.example.brainstationtest.dto.UserConsumerDto;
import com.example.brainstationtest.dto.UserDto;
import com.example.brainstationtest.entity.User;
import com.example.brainstationtest.repository.UserRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class UserService {
    private final UserRepo userRepo;
    private KafkaTemplate<String, UserConsumerDto> kafkaTemplate;

    @CacheEvict(value = "userList", allEntries = true)
    public UserDto save(UserDto dto) {
        User user = new User();
        BeanUtils.copyProperties(dto, user);
        user = userRepo.save(user);
        BeanUtils.copyProperties(user, dto);

        UserConsumerDto userConsumerDto=new UserConsumerDto("Created",dto);
        kafkaTemplate.send("user-events",userConsumerDto);
        return dto;
    }
    @Cacheable(value = "user", key = "#id", unless = "#result==null")
    public UserDto getUser(Long id) {
        log.info("Get All Users Data From Database");
        Optional<User> optionalUser = userRepo.findById(id);
        if (optionalUser.isEmpty()) {
            return null;
        } else {
            UserDto dto = new UserDto();
            BeanUtils.copyProperties(optionalUser.get(), dto);
            return dto;
        }
    }

    public boolean getUserByUsername(String username) {
        return userRepo.existsByUsername(username);
    }

    @Cacheable(value = "userList", unless = "#result==null or #result.isEmpty()")
    public List<UserDto> getAllUser() {
        log.info("Get User Data From Database");
        List<User> userList = userRepo.findAll();
        List<UserDto> userDtoList = new ArrayList<>();
        userList.forEach(user -> {
            UserDto dto = new UserDto();
            BeanUtils.copyProperties(user, dto);
            userDtoList.add(dto);
        });
        return userDtoList;
    }

    @Caching(
            evict = {@CacheEvict(value = "userList", allEntries = true)},
            put = {@CachePut(value = "user", key = "#dto.id")}
    )
    public UserDto update(UserDto dto) {
        User user = new User();
        BeanUtils.copyProperties(dto, user);
        user = userRepo.save(user);
        BeanUtils.copyProperties(user, dto);

        UserConsumerDto userConsumerDto=new UserConsumerDto("Updated",dto);
        kafkaTemplate.send("user-events",userConsumerDto);
        return dto;
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "userList", allEntries = true),
                    @CacheEvict(value = "user", key = "#id")
            }
    )
    public void delete(Long id) {
        Optional<User> optionalUser = userRepo.findById(id);
        UserDto dto = new UserDto();
        BeanUtils.copyProperties(optionalUser.get(), dto);

        userRepo.deleteById(id);

        UserConsumerDto userConsumerDto=new UserConsumerDto("Deleted",dto);
        kafkaTemplate.send("user-events",userConsumerDto);
    }


    @KafkaListener(topics = "user-events",groupId = "my-groupId")
    private void userConsumer(UserConsumerDto consumerDto){
        log.info(consumerDto.getMessage());
        log.info("-----------------------------------------------------------");

        log.info(consumerDto.getUserDto().getUsername());
        log.info(consumerDto.getUserDto().getEmail());
        log.info(consumerDto.getUserDto().getName());
        log.info("-----------------------------------------------------------");
    }

}
