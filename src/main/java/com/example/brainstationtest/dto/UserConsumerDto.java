package com.example.brainstationtest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserConsumerDto {
    private String message;
    private UserDto userDto;
}
