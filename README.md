# Brain Station-23 Test Project


## Technology use in Project

- Spring Boot
- MySQL
- Kafka
- Redis
- Docker


## Instruction to create Application Image From Docker File

- Create .jar file using the following command
```
mvn clean install
```
- Use the following command from the `Dockerfile` file directory
```
docker build -t <image_name> .
```
- Use the following command to run the image
```
docker run -p <host_port>:<container_port> --name <container_name> <image_name> 
```


## Instruction to build and run Application From Docker Compose File

- Use the following command from the `docker-compose.yml` file directory
```
docker compose up -d
```


> **_Note :_** Check your `Host Ip` address and place it in `application.properties` file.
